import React, { useState } from "react";
import LOGO from "../../assets/images/logoMin2.png";
import img1 from "../../assets/images/caroussel1.png";
import img2 from "../../assets/images/caroussel2.png";
import img3 from "../../assets/images/caroussel3.png";
import img4 from "../../assets/images/caroussel4.png";

import emj1 from "../../assets/icons/emojieAdorer.png";
import emj2 from "../../assets/icons/emojieApplaudir.png";
import emj3 from "../../assets/icons/emojieCool.png";
import emj4 from "../../assets/icons/emojieLike.png";
import emj5 from "../../assets/icons/emojieSupris.png";
import emj6 from "../../assets/icons/emojieMecontent.png";
import africAdd from "../../assets/icons/iconeMakeAfricaAdd.png";
import socio1 from "../../assets/icons/iconLien.png";
import socio2 from "../../assets/icons/iconeWhatsapp.png";
import socio3 from "../../assets/icons/iconFacebook.png";
import socio4 from "../../assets/icons/iconeTwitter.png";

import Box from "@mui/material/Box";
import Drawer from "@mui/material/Drawer";
import iconeCommentaire from "../../assets/icons/iconEmojie.png";
import iconePartage from "../../assets/icons/iconPartager.png";

import Slider from "react-slick";

const Accueil = () => {
  const [tableauEmojies, setTableauEmojies] = useState([
    {
      id: 1,
      emojie: emj1,
    },
    {
      id: 2,
      emojie: emj5,
    },
    {
      id: 3,
      emojie: emj2,
    },
    {
      id: 4,
      emojie: emj6,
    },
    {
      id: 5,
      emojie: emj4,
    },
    {
      id: 5,
      emojie: emj3,
    },
  ]);
  const [listeCaroussel, setListeCaroussel] = useState([
    {
      id: 1,
      image: img1,
    },
    {
      id: 2,
      image: img2,
    },
    {
      id: 3,
      image: img3,
    },
    {
      id: 4,
      image: img4,
    },
  ]);
  var settings = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
  };

  const [ListeReseauSociaux, setReseauSociaux] = useState([
    {
      id: 1,
      image: socio1,
      designation: "Copie le lien",
    },
    {
      id: 2,
      image: socio2,
      designation: "Whatsapp",
    },
    {
      id: 3,
      image: socio3,
      designation: "Facebook",
    },
    {
      id: 4,
      image: socio4,
      designation: "Twitter",
    },
  ]);

  const [state, setState] = useState({
    top: false,
    left: false,
    bottom: false,
    right: false,
  });

  const toggleDrawer = (anchor, open) => (event) => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }

    setState({ ...state, [anchor]: open });
  };

  const list = (anchor) => (
    <Box
      sx={{ width: anchor === "top" || anchor === "bottom" ? "auto" : 200 }}
      lg={{ width: anchor === "top" || anchor === "right" ? "auto" : 200 }}
      xl={{ width: anchor === "top" || anchor === "right" ? "auto" : 200 }}
      md={{ width: anchor === "top" || anchor === "right" ? "auto" : 200 }}
      role="presentation"
      onClick={toggleDrawer(anchor, false)}
      onKeyDown={toggleDrawer(anchor, false)}
    >
      <div className="flex flex-col gap-4 w-[95%] mx-auto py-3  ">
        <div className="flex flex-row justify-between items-center w-full px-2">
          <h2 className="text-[20px] font-medium">Partager</h2>
          <span
            onClick={toggleDrawer(anchor, false)}
            onKeyDown={toggleDrawer(anchor, false)}
            className="font-bold cursor-pointer text-[25px]"
          >
            x
          </span>
        </div>

        <div className="grid grid-cols-2 lg:w-[90%] xl:w-[90%] md:w-[90%] w-[70%] mx-auto gap-4">
          {ListeReseauSociaux.map((socio) => (
            <div className=" flex flex-col items-center gap-2 w-full  px-2 cursor-pointer">
              <img src={socio.image} alt="" />
              <h3 className="font-semibold">{socio.designation}</h3>
            </div>
          ))}
        </div>
      </div>
    </Box>
  );
  return (
    <div className="w-full h-full flex  flex-col bg-white">
      <div className="fixed top-0 z-50  w-full   ">
        <div className="navbar bg-[#ffffffde]">
          <div className="navbar-start">
            <img src={LOGO} className="w-16 navbar-center" />
          </div>

          <div className="navbar-end pr-3">
            <h3 className="text-[24px] font-semibold">Spidi</h3>
          </div>
        </div>
      </div>
      <div className="mt-[4rem] mb-[3rem] w-full  h-full">
        <Slider {...settings}>
          {listeCaroussel.map((item) => (
            <div className="w-full  flex justify-center items-center">
              <img
                src={item.image}
                alt="caroussel"
                className="h-[520px] xl:h-[700px] lg:h-[700px] md:h-[700px] w-full "
              />
            </div>
          ))}
        </Slider>

        <div className="flex flex-col gap-4">
          <div className="flex w-full flex-row my-3 justify-between items-center px-2">
            <div className="dropdown dropdown-top">
              <button
                tabIndex={0}
                className="flex flex-row justify-start items-center gap-2"
              >
                <img src={iconeCommentaire} alt="" />
                <span className="text-[18px] font-semibold">25 k</span>
              </button>
              <ul
                tabIndex={0}
                className="dropdown-content   bg-base-100 rounded-box  mb-[2rem] z-[1] w-[350px] shadow"
              >
                <div className="w-[90%] mx-auto flex  flex-row  justify-between items-center">
                  {tableauEmojies.map((items) => (
                    <img src={items.emojie} alt="" />
                  ))}
                </div>
              </ul>
            </div>

            <button
              onClick={toggleDrawer("bottom", true)}
              className=" lg:hidden xl:hidden md:hidden flex flex-row justify-start items-center gap-2"
            >
              <img src={iconePartage} alt="" />
              <span className="text-[18px] font-semibold">600 </span>
            </button>
            <button
              onClick={toggleDrawer("right", true)}
              className=" hidden lg:flex xl:flex md:flex  flex-row justify-start items-center gap-2"
            >
              <img src={iconePartage} alt="" />
              <span className="text-[18px] font-semibold">600 </span>
            </button>
          </div>

          <hr className="" />

          <div className="w-full px-3 font-medium gap-x-3">
            <h2 className="text-[16px]">
              L'Afrique le soleil du monde - 600 vues
            </h2>

            <div className="w-full gap-3  my-4 flex flex-row gap-y-2 justify-start items-center">
              <img src={africAdd} alt="caroussel" className=" " />
              <h2 className="text-[16px] font-semibold">Make Africa Great</h2>
            </div>
          </div>
        </div>
      </div>

      <Drawer
        anchor="bottom"
        open={state.bottom}
        onClose={toggleDrawer("bottom", false)}
      >
        {list("bottom")}
      </Drawer>

      <Drawer
        anchor="right"
        open={state.right}
        onClose={toggleDrawer("right", false)}
      >
        <div className="w-[300px]">{list("bottom")}</div>
      </Drawer>
    </div>
  );
};

export default Accueil;
