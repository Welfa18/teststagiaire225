import React, { useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import logoSpidi from "../../assets/images/logoSpidi.png";
import iconArrow from "../../assets/icons/iconeArrow.png";
import iconCI from "../../assets/icons/iconCI.png";
import iconNigeria from "../../assets/icons/iconNigeria.png";
import iconKenya from "../../assets/icons/iconKenya.png";
import iconCameroun from "../../assets/icons/iconCameroun.png";
import iconBenin from "../../assets/icons/iconBenin.png";

import loginImage from "../../assets/images/image1.png";
import loginImag2 from "../../assets/images/image2.png";
import loginImag3 from "../../assets/images/image3.png";
import { ThreeDots } from "react-loader-spinner";

import Box from "@mui/material/Box";
import Drawer from "@mui/material/Drawer";
import FilledInput from "@mui/material/FilledInput";

import InputAdornment from "@mui/material/InputAdornment";

import FormControl from "@mui/material/FormControl";
import IconButton from "@mui/material/IconButton";

import Visibility from "@mui/icons-material/Visibility";
import VisibilityOff from "@mui/icons-material/VisibilityOff";

const Login = () => {
  const navigate = useNavigate();
  const [isLoading, setIsLoading] = useState(false);
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [UserEmail, setUserEmail] = useState("");
  const [Research, setResearch] = useState("");

  const [UserTel, setUserTel] = useState("");
  const [pays, setPays] = useState("");
  const [selectPays, setSelectPays] = useState("");
  const [messageAlert, setMessageAlert] = useState("");
  const [ListePays, setListePays] = useState([
    { id: 1, name: "Côte d'ivoire", code: "+225", drapeau: iconCI },
    { id: 2, name: "Nigéria", code: "+234", drapeau: iconNigeria },
    { id: 3, name: "Kenya", code: "+254", drapeau: iconKenya },
    { id: 4, name: "Cameroun", code: "+237", drapeau: iconCameroun },
    { id: 5, name: "Bénin", code: "+229", drapeau: iconBenin },
  ]);

  const handleSubmit = () => {
    setIsLoading(true);
    const data = {
      password: password,
      email: username,
    };

    setTimeout(() => {
      navigate("/acceuil");
    }, 5000);
  };

  const [state, setState] = useState({
    top: false,
    left: false,
    bottom: false,
    right: false,
  });

  const toggleDrawer = (anchor, open) => (event) => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }

    setState({ ...state, [anchor]: open });
  };
  const [showPassword, setShowPassword] = React.useState(false);

  const handleClickShowPassword = () => setShowPassword((show) => !show);

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  // Fonction de filtrage
  const filteredPays = ListePays.filter((pays) =>
    pays.name.toLowerCase().includes(Research.toLowerCase())
  );

  const list = (anchor) => (
    <Box
      sx={{ width: anchor === "top" || anchor === "bottom" ? "auto" : 200 }}
      lg={{ width: anchor === "top" || anchor === "right" ? "auto" : 200 }}
      xl={{ width: anchor === "top" || anchor === "right" ? "auto" : 200 }}
      md={{ width: anchor === "top" || anchor === "right" ? "auto" : 200 }}
      role="presentation"
      onClick={(e) => {
        e.stopPropagation();
        toggleDrawer(anchor, false)(e);
      }}
      onKeyDown={(e) => {
        e.stopPropagation();
        toggleDrawer(anchor, false)(e);
      }}
    >
      <div className="flex flex-col gap-4 w-[95%]  px-2 mx-auto py-3  ">
        <div className="flex justify-center items-center w-full ">
          <div className="h-1 rounded-xl w-[40px] bg-[#DADADA]"></div>
        </div>
        <label
          onClick={(e) => {
            e.stopPropagation();
          }}
          className="input input-bordered  flex items-center gap-2"
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 16 16"
            fill="currentColor"
            className="h-4 w-4 opacity-70"
          >
            <path
              fillRule="evenodd"
              d="M9.965 11.026a5 5 0 1 1 1.06-1.06l2.755 2.754a.75.75 0 1 1-1.06 1.06l-2.755-2.754ZM10.5 7a3.5 3.5 0 1 1-7 0 3.5 3.5 0 0 1 7 0Z"
              clipRule="evenodd"
            />
          </svg>
          <input
            type="text"
            className="grow"
            value={Research}
            onChange={(e) => {
              e.stopPropagation();
              setResearch(e.target.value);
            }}
            placeholder="Rechercher un pays"
          />
        </label>
        <div className="flex flex-col gap-4">
          {filteredPays.map((pays) => (
            <div
              key={pays.code}
              onClick={(e) => {
                e.stopPropagation();
                setSelectPays(pays);
                setUserTel(pays.code);
              }}
              className="flex flex-row justify-between w-full px-2 cursor-pointer"
            >
              <div className="flex flex-row justify-start items-center gap-2">
                <img src={pays.drapeau} alt={`Drapeau de ${pays.name}`} />
                <h4 className="text-[#18171E] font-semibold text-[16px]">
                  {pays.name}
                </h4>
              </div>
              <h3 className="text-[#18171E] font-semibold text-[16px]">
                {pays.code}
              </h3>
            </div>
          ))}
        </div>
      </div>
    </Box>
  );

  return (
    <div className="w-full min-h-screen flex items-center">
      <div className="block lg:hidden md:hidden  h-full w-full mx-2  md:h-full bg-white p-4 rounded-lg">
        <div className="flex flex-col justify-center items-center w-full h-full rounded-lg">
          <div className="flex flex-col ml-[-2rem]  mt-[3rem] justify-center items-center ">
            <img src={logoSpidi} alt="Logo de Spidi" className="w-[170px]" />
            <h1 className="text-[45px] ml-8 text-black font-semibold mt-2">
              Spidi
            </h1>
          </div>

          <div className="mt-5 w-full h-full  ">
            <div className="form-control w-full gap-4">
              <input
                type="text"
                value={username}
                onChange={(e) => {
                  setUsername(e.target.value);
                }}
                placeholder="Nom d'utilisateur"
                className=" py-[14px] px-[15px] w-full font-medium border-[1px] border-[#939393] text-black bg-[#F6F6F6] rounded-[14px]"
              />
              <div
                className="py-[14px] px-[15px] flex flex-row justify-between w-full font-medium border-[1px] border-[#939393] text-[#939393] bg-[#F6F6F6] rounded-[14px] cursor-pointer"
                onClick={toggleDrawer("bottom", true)}
              >
                <h2>
                  {selectPays ? (
                    <span className="text-black">{selectPays.name}</span>
                  ) : (
                    "Votre pays"
                  )}
                </h2>
                <img src={iconArrow} alt="arrow " />
              </div>
              <Drawer
                anchor="bottom"
                open={state.bottom}
                onClose={toggleDrawer("bottom", false)}
              >
                {list("bottom")}
              </Drawer>

              <input
                type="tel"
                value={UserTel}
                onChange={(e) => setUserTel(e.target.value)}
                placeholder="+000 Numéro de téléphone"
                className=" py-[14px] px-[15px] w-full font-medium border-[1px] border-[#939393] text-black bg-[#F6F6F6] rounded-[14px]"
              />
              <input
                type="email"
                value={UserEmail}
                onChange={(e) => setUserEmail(e.target.value)}
                placeholder="Email"
                className=" py-[14px] px-[15px] w-full font-medium border-[1px] border-[#939393] text-black bg-[#F6F6F6] rounded-[14px]"
              />

              <FormControl sx={{ m: 1, width: "25ch" }} variant="filled">
                <FilledInput
                  id="filled-adornment-password"
                  type={showPassword ? "text" : "password"}
                  placeholder="Mot de passe"
                  className="py-[14px] px-[15px] w-full font-medium border-[1px] border-[#939393] text-black bg-[#F6F6F6] rounded-[14px]"
                  endAdornment={
                    <InputAdornment position="end">
                      <IconButton
                        aria-label="toggle password visibility"
                        onClick={handleClickShowPassword}
                        onMouseDown={handleMouseDownPassword}
                        edge="end"
                      >
                        {showPassword ? <VisibilityOff /> : <Visibility />}
                      </IconButton>
                    </InputAdornment>
                  }
                />
              </FormControl>
            </div>

            {messageAlert ? (
              <div className="mt-4">
                <p className="text-red-500 text-sm text-center font-medium">
                  {messageAlert}
                </p>
              </div>
            ) : null}
            <button
              onClick={handleSubmit}
              className="mt-[3rem] bg-[#18171E] py-[17px] px-[31px] flex items-center justify-center w-full h-10 font-medium text-white rounded-[16px] hover:drop-shadow-md"
            >
              {!isLoading ? (
                <span className="text-[16px]">Dream big make big</span>
              ) : (
                <ThreeDots
                  height="40"
                  width="40"
                  radius="9"
                  color="#fff"
                  ariaLabel="three-dots-loading"
                  wrapperStyle={{}}
                  wrapperClassName=""
                  visible={isLoading}
                />
              )}
            </button>
          </div>
        </div>
      </div>

      <div className="w-full h-full hidden xl:flex  lg:flex md:flex flex-row">
        <div className="hidden xl:flex justify-center items-center lg:flex md:flex :w-1/2  lg:min-h-screen md:w-1/2  md:min-h-screen bg-white">
          <div className="w-[80%] md:[70%]  lg:w-[70%] xl:w-[70%]  mx-auto h-fit rounded-lg">
            <div className="flex flex-col justify-center items-center w-full h-full rounded-lg">
              <div className="flex flex-col ml-[-2rem]  justify-center items-center ">
                <img
                  src={logoSpidi}
                  alt="Logo de Spidi"
                  className="w-[170px]"
                />
                <h1 className="text-[45px] ml-8 text-black font-semibold mt-2">
                  Spidi
                </h1>
              </div>

              <div className="mt-5 w-full h-full  ">
                <div className="form-control w-full gap-4">
                  <input
                    type="text"
                    value={username}
                    onChange={(e) => setUsername(e.target.value)}
                    placeholder="Nom d'utilisateur"
                    className=" py-[14px] px-[15px] w-full font-medium border-[1px] border-[#939393] text-black bg-[#F6F6F6] rounded-[14px]"
                  />
                  <div
                    className="py-[14px] px-[15px] lg:hidden xl:hidden md:hidden  flex flex-row justify-between w-full font-medium border-[1px] border-[#939393] text-[#939393] bg-[#F6F6F6] rounded-[14px] cursor-pointer"
                    onClick={toggleDrawer("bottom", true)}
                  >
                    <h2>
                      {selectPays ? (
                        <span className="text-black">{selectPays.name}</span>
                      ) : (
                        "Votre pays"
                      )}
                    </h2>
                    <img src={iconArrow} alt="arrow " />
                  </div>
                  <div
                    className="py-[14px] px-[15px] hidden lg:flex xl:flex md:flex flex-row justify-between w-full font-medium border-[1px] border-[#939393] text-[#939393] bg-[#F6F6F6] rounded-[14px] cursor-pointer"
                    onClick={toggleDrawer("right", true)}
                  >
                    <h2>
                      {selectPays ? (
                        <span className="text-black">{selectPays.name}</span>
                      ) : (
                        "Votre pays"
                      )}
                    </h2>
                    <img src={iconArrow} alt="arrow " />
                  </div>
                  <Drawer
                    anchor="bottom"
                    open={state.bottom}
                    onClose={toggleDrawer("bottom", false)}
                  >
                    {list("bottom")}
                  </Drawer>

                  <Drawer
                    anchor="right"
                    open={state.right}
                    onClose={toggleDrawer("right", false)}
                  >
                    <div className="w-[300px]">{list("bottom")}</div>
                  </Drawer>

                  <input
                    type="tel"
                    value={UserTel}
                    onChange={(e) => setUserTel(e.target.value)}
                    placeholder="+000 Numéro de téléphone"
                    className=" py-[14px] px-[15px] w-full font-medium border-[1px] border-[#939393] text-black bg-[#F6F6F6] rounded-[14px]"
                  />
                  <input
                    type="email"
                    value={UserEmail}
                    onChange={(e) => setUserEmail(e.target.value)}
                    placeholder="Email"
                    className=" py-[14px] px-[15px] w-full font-medium border-[1px] border-[#939393] text-black bg-[#F6F6F6] rounded-[14px]"
                  />

                  <FormControl sx={{ m: 1, width: "25ch" }} variant="filled">
                    <FilledInput
                      id="filled-adornment-password"
                      type={showPassword ? "text" : "password"}
                      placeholder="Mot de passe"
                      className="py-[14px] px-[15px] w-full font-medium border-[1px] border-[#939393] text-black bg-[#F6F6F6] rounded-[14px]"
                      endAdornment={
                        <InputAdornment position="end">
                          <IconButton
                            aria-label="toggle password visibility"
                            onClick={handleClickShowPassword}
                            onMouseDown={handleMouseDownPassword}
                            edge="end"
                          >
                            {showPassword ? <VisibilityOff /> : <Visibility />}
                          </IconButton>
                        </InputAdornment>
                      }
                    />
                  </FormControl>
                </div>

                {messageAlert ? (
                  <div className="mt-4">
                    <p className="text-red-500 text-sm text-center font-medium">
                      {messageAlert}
                    </p>
                  </div>
                ) : null}
                <button
                  onClick={handleSubmit}
                  className="mt-[3rem] bg-[#18171E] py-[17px] px-[31px] flex items-center justify-center w-full h-10 font-medium text-white rounded-[16px] hover:drop-shadow-md"
                >
                  {!isLoading ? (
                    <span className="text-[16px]">Dream big make big</span>
                  ) : (
                    <ThreeDots
                      height="40"
                      width="40"
                      radius="9"
                      color="#fff"
                      ariaLabel="three-dots-loading"
                      wrapperStyle={{}}
                      wrapperClassName=""
                      visible={isLoading}
                    />
                  )}
                </button>
              </div>
            </div>
          </div>
        </div>
        <div className="hidden xl:flex flex-col  justify-center items-center gap-10 border-l-2 lg:flex md:flex :w-1/2  lg:min-h-screen md:w-1/2  md:min-h-screen bg-[#]">
          <h2 className="text-[25px] font-medium ">
            Bienvenue sur la page de connexion
          </h2>

          <img src={loginImag3} className="" alt="" />
        </div>
      </div>
    </div>
  );
};

export default Login;
