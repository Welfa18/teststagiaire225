import React from "react";
import { createHashRouter } from "react-router-dom";
import Login from "../views/login/Login";
import Accueil from "../views/accueil/Accueil";
const Router = createHashRouter([
  {
    path: "/",
    element: <Login />,
  },
  {
    path: "/acceuil",
    element: <Accueil />,
  },
]);

export default Router;
